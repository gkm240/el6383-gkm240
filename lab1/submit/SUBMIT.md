
1) The following shows the `ifconfig` command and its output for the client and server in my experiment. 

Client:

gevmanek@client:~$ sudo ifconfig
eth0      Link encap:Ethernet  HWaddr 02:eb:b3:a2:e3:39
          inet addr:172.17.2.6  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::eb:b3ff:fea2:e339/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:474 errors:0 dropped:0 overruns:0 frame:0
          TX packets:482 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:65843 (65.8 KB)  TX bytes:55060 (55.0 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:07:5e:36:9b:9f
          inet addr:10.1.1.1  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::7:5eff:fe36:9b9f/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:31 errors:0 dropped:0 overruns:0 frame:0
          TX packets:14 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:2124 (2.1 KB)  TX bytes:1396 (1.3 KB)
          Interrupt:26

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
          
Server :

gevmanek@server:~$ sudo ifconfig
eth0      Link encap:Ethernet  HWaddr 02:b2:3c:da:0c:62
          inet addr:172.17.2.7  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::b2:3cff:feda:c62/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:388 errors:0 dropped:0 overruns:0 frame:0
          TX packets:415 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:55746 (55.7 KB)  TX bytes:48448 (48.4 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:5f:c5:77:fa:d7
          inet addr:10.1.1.2  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::5f:c5ff:fe77:fad7/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:22 errors:0 dropped:0 overruns:0 frame:0
          TX packets:13 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:1288 (1.2 KB)  TX bytes:1306 (1.3 KB)
          Interrupt:26

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
```


2)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Node    | control interface name | control IP | data interface name | data IP
------- | ---------------------- | ---------- | ------------------- | --------
client  |      eth0              | 172.17.2.6 |    eth1             | 10.1.1.1
server  |      eth0              | 172.17.2.7 |    eth1             | 10.1.1.2

3)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Link     |  Bandwidth (Mbits/sec)
-------- | ----------------------
control  |   2.64 Gbits/sec
data     |   101 Mbits/sec 


4) What happens if you bring down the data interface of your node using the command

    sudo ifconfig DATAIFNAME down

 where `DATAIFNAME` is the name of the data interface (i.e. `eth0` or `eth1`)? What happens if you bring down the control interface of your node? Why?
 
 
 The Data Interface is used to communicate with other hosts in the environment. Hence,when the data interface is brought down, the pings indicate that the interface is unreachable.
 
 The Control Interface is used to access the host through SSH. Hence when it is brought down, the ability to access the node is lost.  
 
 
