
Lab 3: Routing and resiliency
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 3 - Routing and resiliency
-------------- | --------------------------------
Name           |   Gev Manekshaw
Net ID         |   gkm240
Report due     | Sunday, 29 March 11:59PM


Please answer the following questions:


## Dijkstra's algorithm experiment

1. Were you able to successfully produce experiment results? If so, show a screenshot of your experiment results (topology + completed table).
The experiment results were successfully produced. The topology assigned, and the completed table are shown in the links.
http://imgur.com/p9y5hZF
http://imgur.com/Xrv3XQn

2. How long did it take you to run this experiment, from start (create an account) to finish (getting a screenshot of results)?
It took approximately 30 min to run the experiment.  

3. Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
No extra steps were required to successfully complete this experiment.

4. In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
The degree of reproducibility of this experiment is 4. This because propriety packages like access to GENI testbed is required, and not much effort on the users part is required.

5. Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if there was no web-based interface on hyperion.poly.edu?
If the experiment is to be recreated with the given resources, it would take approximately 45 min to setup and run the experiment.

## OSPF experiment

1. Were you able to successfully produce experiment results? If so, show your experiment results. You should have:
 * Traceroute from client to server with all links active
 gevmanek@client:~$ traceroute server
traceroute to server (192.168.20.10), 30 hops max, 60 byte packets
 1  router-1-lan5 (192.168.10.10)  0.875 ms  0.834 ms  0.800 ms
 2  router-4-lan3 (192.168.4.1)  1.210 ms  1.195 ms  1.162 ms
 3  router-3-lan1 (192.168.2.2)  1.969 ms  1.948 ms  1.917 ms
 4  server-lan6 (192.168.20.10)  2.463 ms  2.437 ms  2.401 ms

 * OSPF table on router-1 with all links active
 gevmanek@router-1:/local/xorp_run/logs$ cat last_table.txt
Welcome to XORP v1.8.5 on router-1.lab3-gkm240.ch-geni-net.instageni.rnet.missou
ri.edu
Version tag: 00000000  Build Date: 2014-06-19 11:41 64-bit
root@router-1.lab3-gkm240.ch-geni-net.instageni.rnet.missouri.edu> show route ta
ble ipv4 unicast ospf
192.168.2.0/24  [ospf(110)/20]
                > to 192.168.1.2 via eth3/eth3
192.168.3.0/24  [ospf(110)/20]
                > to 192.168.4.1 via eth1/eth1
192.168.5.0/24  [ospf(110)/20]
                > to 192.168.1.2 via eth3/eth3
192.168.20.0/24 [ospf(110)/30]
                > to 192.168.4.1 via eth1/eth1

 * Traceroute from client to server with the router-2 link down
 gevmanek@client:~$ traceroute server
traceroute to server (192.168.20.10), 30 hops max, 60 byte packets
 1  router-1-lan5 (192.168.10.10)  0.598 ms  0.555 ms  0.685 ms
 2  router-4-lan3 (192.168.4.1)  1.021 ms  0.994 ms  0.962 ms
 3  router-3-lan2 (192.168.3.2)  1.436 ms  1.409 ms  1.425 ms
 4  server-lan6 (192.168.20.10)  1.780 ms  1.753 ms  1.721 ms

 * OSPF table on router-1 with the router-2 link down
 gevmanek@router-1:/local/xorp_run/logs$ cat last_table.txt
Welcome to XORP v1.8.5 on router-1.lab3-gkm240.ch-geni-net.instageni.rnet.missou
ri.edu
Version tag: 00000000  Build Date: 2014-06-19 11:41 64-bit
root@router-1.lab3-gkm240.ch-geni-net.instageni.rnet.missouri.edu> show route ta
ble ipv4 unicast ospf
192.168.2.0/24  [ospf(110)/30]
                > to 192.168.4.1 via eth1/eth1
192.168.3.0/24  [ospf(110)/20]
                > to 192.168.4.1 via eth1/eth1
192.168.5.0/24  [ospf(110)/20]
                > to 192.168.4.1 via eth1/eth1
192.168.20.0/24 [ospf(110)/30]
                > to 192.168.4.1 via eth1/eth1


2. How long did it take you to run this experiment, from start (reserve resources on GENI) to finish (getting the output to put in the previous )?
A duration of 1 hour was required to run this experiment.

3. Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
The two modifications in the procedure were:
1.	Non Jacks Add Resources were used to reserve non-compute resources.
2.	The OSPF table was observed using the command “cat” to view the log text files. 


4. In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
The degree of reproducibility of this experiment is 4. This because propriety packages like access to GENI testbed is required, and a little effort on the user’s part is required to figure out alternative certain steps.

5. Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if the experiment artifacts (disk image, RSpec, etc.) were not provided for you?
It would approximately take 2 hours to design the experiment.