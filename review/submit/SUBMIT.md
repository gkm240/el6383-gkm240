
Project Review
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment       | Project Review
-------------------- | --------------------------------
Name                 | Gev Manekshaw	
Net ID               | gkm240
Assigned project (#) | 13
Review due           | Monday, 4 May 11:59PM


Please write detailed comments answering the questions below.
Do not just answer "yes" or "no" - comment on *how well* the authors
address each of these aspects of experimentation, and offer 
suggestions for improvement.

## Overview

1) Briefly summarize the experiment in this project.

The experiment involves the comparision of the widely used traffic policies used in the internet : "Policing" and "Shaping". The goal is to identify which of these yields the actual throughput of a connection to match the desired limit. 

-------------------------------------------------------------------------------------------

2) Does the project generally follow the guidelines and parameters we have 
learned in class? 

This project adheres to the guidelines and parameters, by having a well defined goal, being reproduceable and effectively communicating results. The goal selected is focused on comparing a particular property of the services mentioned. Also this project is of relevance because these policies are widely used in the Internet. 

-------------------------------------------------------------------------------------------

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal is to identify which of the traffic policies used in the internet, i.e. "Policing" and "Shaping", yields the actual throughput of a connection to match the desired limit. The goal is well defined and focused, since it involves comparing two variatians of a service. 

-------------------------------------------------------------------------------------------

2) Are the metric(s) and parameters chosen appropriate for this 
experiment goal? Does the experiment design clearly support the experiment goal?

The metric chosen is throughput, while the parameters varied are maximum rate and burst of a connection. Though varying the maximum rate between two nodes is justified, the varying of burst remains ambigous. An important characterestic of "Policing" is that it propagates bursts and does no smoothing, while "Shaping" controls bursts by smoothing the output rate over time intervals. This phenomenon is not introduced in the project.

-------------------------------------------------------------------------------------------

3) Is the experiment well designed to obtain maximum information with the 
minimum number of trials?

The experiment yields the desired and anticipted results without having to repeat any steps of reproducability.

-------------------------------------------------------------------------------------------

4) Are the metrics selected for study the *right* metrics? Are they clear, 
unambiguous, and likely to lead to correct conclusions? Are there other 
metrics that might have been better suited for this experiment?

The chosen metric, throughput, is apt for the experiment since the actual throughput that is achieved after implementing traffic policies is compared to the set link capacity.

-------------------------------------------------------------------------------------------


5) Are the parameters of the experiment meaningful? Are the ranges 
over which parameters vary meaningful and representative?

The two parameters varied are maximum rate and burst of a connection. The maximum rate is rightly varied over the required range to check its closeness to the desired capacity. However, the varying of burst for different maximim rates remains ambigous.    

-------------------------------------------------------------------------------------------


6) Have the authors sufficiently addressed the possibility of interactions 
between parameters?

No possibilty of interactions is addressed in the report.


-------------------------------------------------------------------------------------------

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate 
and realistic? 

The comparisions made are valid. This is because policing and shaping are two variations of traffic policies. Comparing them is useful to indicate which policy is more suitable to use in the internet. 

-------------------------------------------------------------------------------------------


## Communicating results


1) Do the authors report the quantitative results of their experiment?

The quantitative results of the throughput obtained is effectively communicated by bar graphs using the R tool. 

-------------------------------------------------------------------------------------------


2) Is there information given about the variation and/or distribution of 
experimental results?

No information regarding the variation and/or distribution of the throughput measured is given by the authors. The measurements are taken over a single trial. 

-------------------------------------------------------------------------------------------


3) Do the authors practice *data integrity* - telling the truth about their data, 
avoiding ratio games and other practices to artificially make their results seem better?

The authors practice data integrity by only communicating quantative results that have been obtained after running the tests. All the data used to plot the graph has been directly used by saving the results from the commands. No modifications have been made to the data.

-------------------------------------------------------------------------------------------


4) Is the data presented in a clear and effective way? If the data is presented in 
graphical form, is the type of graph selected appropriate for the "story" that 
the data is telling? 

The multiple bar graphs used effectively compare the throughput obtained by varying the maximum rate, for both policing and shaping. The comparision between the two is clearly indicated through the graph.
 
-------------------------------------------------------------------------------------------



5) Are the conclusions drawn by the authors sufficiently supported by the 
experiment results?

The conclusion drawn by the authors is that the results from shaping are closest to the set limit. From the graphs, this conclusion can most definately be drawn. The plot of Bandwidth limit rate vs. throughput shows that the result obtained from shaping is better than that from policing, and is more close to the desired rate.

-------------------------------------------------------------------------------------------

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, 
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The authors have given a comprehensive list of steps to set up the experiment. The steps are clear and easy to understand. 

-------------------------------------------------------------------------------------------


2) Were you able to successfully produce experiment results? 

The results were successfully obtained from the report, and were coherent to that indicated by the authors.

-------------------------------------------------------------------------------------------



3) How long did it take you to run this experiment, from start to finish?

The time to reproduce this experiment was 2 hours.

-------------------------------------------------------------------------------------------


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

Most of the experiment was carried out using the steps listed by the authors. In the part to run the R script, a more familiar method was used to reproduce the experiment. A new node was created that was used to host the R tool. This deviation from the steps in the report did not make much of a difference in terms of time. Modifications had to be made in the R script to change the data and file locations. 


-------------------------------------------------------------------------------------------

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

The degree of reproducibility of this experiment is 4. This because propriety packages like access to GENI testbed is required, and a little effort on the reproducer’s part is required to figure out certain nown alternative steps.

-------------------------------------------------------------------------------------------


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

A suggestion to the authors would be that they should have explained the purpose of the many commands used. Though the commands are mentioned, their purpose is not known. A brief explaination/reason could provide the individual reproducing the experiment an alternative known method to set up the experiment. 



